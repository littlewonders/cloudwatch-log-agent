FROM alpine

ENV AWS_REGION=us-east-1 LOG_FILE=/aws.conf LOG_GROUP=changeme

RUN apk update && \
    apk add --no-cache python3 curl && \
    ln -sf /usr/bin/python3 /usr/bin/python && \
    python3 -m ensurepip && \
    mkdir /var/awslogs && mkdir /var/awslogs/state && mkdir /var/awslogs/etc &&  mkdir /var/awslogs/etc/config  && \
    python3 -m pip install awscli-cwlogs && \
    python3 -m pip uninstall setuptools pip -y && \
    apk --purge -v del curl && \
    rm -rf get-pip.py /var/cache/apk/* /root/.cache/* /usr/share/terminfo

ADD boot.sh /var/awslogs

ENTRYPOINT ["/bin/sh", "/var/awslogs/boot.sh"]
