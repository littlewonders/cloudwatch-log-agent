#!/bin/sh

aws configure set plugins.cwlogs cwlogs
aws configure set default.region $AWS_REGION

aws logs push --config-file /awslogs.conf